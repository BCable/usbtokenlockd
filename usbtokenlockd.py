#!/usr/bin/env python

# Author: Brad Cable
# Email: brad@bcable.net
# License: MIT


import os, subprocess, sys, time

# global config
DELAY=1
USER=os.getlogin()
SCRIPT_PATH=os.path.join("/etc/usbtokenlockd", USER)


# check /proc for an existing usbtokenlockd.py process (excluding this one)
def already_running():
	running = 0
	for proc in os.listdir("/proc"):
		if proc.isdigit():
			f = open(os.path.join("/proc", proc, "cmdline"), "r")
			cmdline = f.read()
			f.close()
			cmdline = cmdline.split("\x00")

			if (
				cmdline[0] == "python" and
				cmdline[1].find("usbtokenlockd.py") != -1
			):
				running += 1

	return running > 1


# load serials from /etc/usbtokenlockd/serials.conf
def load_serials():
	serials = []
	try:
		f = open("/etc/usbtokenlockd/serials.conf")
		for line in f:
			if len(line) > 0:
				serials.append(line)

	except IOError:
		pass

	return serials


# repeated process that checks if the USB drive is around
def poller(serials, last_plugged, first):
	is_plugged_in = False
	found_serial = None
	for dev in os.walk("/sys/devices"):
		device = dev[0].rsplit("/")[-1]
		for serial in serials:
			serial = serial.strip()
			if serial == device[0:len(serial)]:
				found_serial = serial
				is_plugged_in = True
				break

	if last_plugged and not is_plugged_in:
		if os.path.exists(SCRIPT_PATH):
			sys.stdout.write(
				"USB Device(s) Removed: Executing User Script ({})\n".format(
					SCRIPT_PATH
				)
			)
			devnull = open("/dev/null", "a")
			proc = subprocess.Popen(
				[SCRIPT_PATH], stdout=devnull, stderr=devnull
			)
			proc.wait()

		else:
			sys.stderr.write(
				"USB Device ({})  Removed: ERROR: ".format(found_serial) +
				"User Script Not Found For User: {}\n".format(USER)
			)

	elif not last_plugged and is_plugged_in:
		if first:
			sys.stdout.write(
				"USB Device ({}) Discovered\n".format(found_serial)
			)
		else:
			sys.stdout.write(
				"USB Device ({}) Rediscovered\n".format(found_serial)
			)

	return is_plugged_in


# main execution
if __name__ == "__main__":
	if already_running():
		sys.stderr.write("Daemon already running, exiting.")
		sys.exit(1)

	serials = load_serials()

	if serials == []:
		print_str = "File '/etc/usbtokenlockd/serials.conf' could not "
		print_str += "be read or is empty\n"
		sys.stderr.write(print_str)
		sys.exit(2)

	last_plugged = False
	first = True

	while True:
		try:
			last_plugged = poller(serials, last_plugged, first)
			if last_plugged:
				first = False

		except Exception:
			pass

		time.sleep(DELAY)
