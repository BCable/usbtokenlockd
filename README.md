usbtokenlockd 1.0
=================

Tool that executes a command (usually a screen lock) when specific USB devices are removed from the machine.

License: MIT

Table of Contents
-----------------

    I: General Usage

    II: Files and Directories

      II.a: /etc/usbtokenlockd

      II.b: /etc/usbtokenlockd/serials.conf

      II.c: /etc/usbtokenlockd/[user]


I. General Usage
----------------

The usbtokenlockd.py script needs to be run in the background however you choose.  This script runs as your local user account, and NOT root.

The script tries to automatically detect if multiple copies are being run.  If another instance is found in the process tree, then it quits.

II: Files and Directories
-------------------------

II.a. /etc/usbtokenlockd
------------------------

Directory that stores the serials.conf file as well as the symbolic links for each users' execution.  Should not be writeable by users, but they will need to be able to read this directory.

II.b. /etc/usbtokenlockd/serials.conf
-------------------------------------

Series of USB serial numbers to check for.  Each serial must be on a separate line.  Formats of serials generally are in a similar form of "0001:2345:6789".

Every single device must be removed before the associated user script is executed.  The only real reason to use multiple serial numbers is if you use multiple devices at different times and wish to configure the daemon once.

II.c. /etc/usbtokenlockd/[user]
-------------------------------

Symbolic link to the script to execute for each user.  If your username is "brad" and the script you want to run is "/home/brad/.i3/scripts/locker.sh", then you'll want to make the symbolic link "/etc/usbtokenlockd/brad" point to "/home/brad/.i3/scripts/locker.sh".
